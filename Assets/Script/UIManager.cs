﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : MonoBehaviour
{
    public GameManager gameManager;
    public CharacterController characterController;
    public FinishPanelScript finishPanelScript;

    //public CounterManager counterManager;

    [Header("Panels")]
    public GameObject MenüPanel;
    public GameObject GamePanel;
    public GameObject FinishPanel;


    public void Awake()
    {

    }
    public void Start()
    {

    }
    public void Update()
    {
        if (gameManager.gameStatus != GameStatus.start)
        {
            return;
        }
        
    }

        public void GameStart()
    {
        //Debug.Log("uı gelmiyor");
        HideAllPanel();
        GamePanel.gameObject.SetActive(true);



        gameManager.StartGame();
    }
    public void GameFinish()
    {
        HideAllPanel();
        FinishPanel.gameObject.SetActive(true);

        gameManager.GameFinish();
    }
    public void GameExit()
    {
        Application.Quit();
    }
    public void HideAllPanel()
    {
        MenüPanel.gameObject.SetActive(false);
        GamePanel.gameObject.SetActive(false);
        FinishPanel.gameObject.SetActive(false);
    }

}
