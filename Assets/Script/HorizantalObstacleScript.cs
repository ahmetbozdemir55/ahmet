﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HorizantalObstacleScript : MonoBehaviour
{
    //public float thrust = 0.0001f;
    public Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Movefront();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(gameObject.transform.position);
        transform.Rotate(new Vector3(0, 10, 0));

    }
    public void OnCollisionEnter(Collision collision)
    {

    }
    public void Movefront()
    {
        if (gameObject.transform.tag=="Horizantal1")
        {
            gameObject.transform.DOMove(new Vector3(4.7f,22.5f,66.1f), 6).OnComplete(Moveback1);
        }
        if (gameObject.transform.tag == "Horizantal4")
        {
            gameObject.transform.DOMove(new Vector3(7.4f, 22.5f, 55.6f), 4).OnComplete(Moveback4);
        }
        if (gameObject.transform.tag == "Horizantal2")
        {
            gameObject.transform.DOMove(new Vector3(-9.1f, 22.5f, 60.9f), 4).OnComplete(Moveback2);
        }
        if (gameObject.transform.tag == "Horizantal3")
        {
            gameObject.transform.DOMove(new Vector3(-2.7f, 22.5f, 77.1f), 5).OnComplete(Moveback3);
        }
    }
    public void Moveback1()
    {
        gameObject.transform.DOMove(new Vector3(-6.8f, 22.5f, 54.6f), 6).OnComplete(Movefront);
    }
    public void Moveback4()
    {
        gameObject.transform.DOMove(new Vector3(-3.2f, 22.5f, 66.9f), 4).OnComplete(Movefront);
    }
    public void Moveback2()
    {
        gameObject.transform.DOMove(new Vector3(3.7f, 22.5f, 73.8f), 4).OnComplete(Movefront);
    }
    public void Moveback3()
    {
        gameObject.transform.DOMove(new Vector3(10f, 22.5f, 65.5f), 5).OnComplete(Movefront);
    }
}
