﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameStatus
{
    menü,
    start,
    finish,
    exit
}

public class GameManager : MonoBehaviour
{
    public int Level;
    //public fireman ballController;
    public GameStatus gameStatus;
    public InputManager inputManager;
    void Awake()
    {
        gameStatus = GameStatus.menü;
    }
    public void Start()
    {

    }

    public void StartGame()
    {
        Invoke("Delay", 1f);
    }
    public void Delay()
    {
        gameStatus = GameStatus.start;

    }
    public void ClickExit()
    {
        gameStatus = GameStatus.exit;
    }
    public void GameFinish()
    {
        gameStatus = GameStatus.finish;

    }
    // Update is called once per frame
    void Update()
    {

    }

}
