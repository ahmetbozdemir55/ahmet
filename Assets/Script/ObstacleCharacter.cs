﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObstacleCharacter : MonoBehaviour
{
    public GameManager gameManager;
    public GameStatus gameStatus;

    public List<List<Transform>> Waypointsdirection;

    public Transform[] waypointsLeft;
    public Transform[] waypointsRight;

    public List<List<Vector3>> WayLeft;
    public List<List<Vector3>> WayRight;

    public Vector3[] LeftwayPoint1;
    public Vector3[] LeftwayPoint2;
    public Vector3[] LeftwayPoint3;
    public Vector3[] LeftwayPoint4;
    public Vector3[] LeftwayPoint5;
    public Vector3[] LeftwayPoint6;
    public Vector3[] LeftwayPoint7;
    public Vector3[] LeftwayPoint8;
    public Vector3[] LeftwayPoint9;
    public Vector3[] LeftwayPoint10;
    public Vector3[] LeftwayPoint11;
    public Vector3[] RightwayPoint1;
    public Vector3[] RightwayPoint2;
    public Vector3[] RightwayPoint3;
    public Vector3[] RightwayPoint4;
    public Vector3[] RightwayPoint5;
    public Vector3[] RightwayPoint6;
    public Vector3[] RightwayPoint7;
    public Vector3[] RightwayPoint8;
    public Vector3[] RightwayPoint9;
    public Vector3[] RightwayPoint10;
    public Vector3[] RightwayPoint11;

    public List<Vector3> selectpath;
    public List<Transform> direction;

    public float moveSpeed ;
    public Rigidbody rb;
    public float thrust;
    public int waypointIndex = 0;

    public bool lastposition=false;
    private Vector3 targetPosition;
    float mindistance = -0.5f;
    float maxdistance = 0.5f;

    private void Start()

    {
        rb = GetComponent<Rigidbody>();
        moveSpeed = Random.Range(6.5f, 10.5f);
        //Debug.Log(gameObject.transform.position);
        Waypointsdirection = new List<List<Transform>>();
        selectpath = new List<Vector3>();


        WayLeft = new List<List<Vector3>>();
        LeftwayPoint1 = new Vector3[12];
        LeftwayPoint2 = new Vector3[12];
        LeftwayPoint3 = new Vector3[12];
        LeftwayPoint4 = new Vector3[12];
        LeftwayPoint5 = new Vector3[12];
        LeftwayPoint6 = new Vector3[12];
        LeftwayPoint7 = new Vector3[12];
        LeftwayPoint8 = new Vector3[12];
        LeftwayPoint9 = new Vector3[12];
        LeftwayPoint10 = new Vector3[12];
        LeftwayPoint11 = new Vector3[12];

        WayRight = new List<List<Vector3>>();
        RightwayPoint1 = new Vector3[12];
        RightwayPoint2 = new Vector3[12];
        RightwayPoint3 = new Vector3[12];
        RightwayPoint4 = new Vector3[12];
        RightwayPoint5 = new Vector3[12];
        RightwayPoint6 = new Vector3[12];
        RightwayPoint7 = new Vector3[12];
        RightwayPoint8 = new Vector3[12];
        RightwayPoint9 = new Vector3[12];
        RightwayPoint10 = new Vector3[12];
        RightwayPoint11 = new Vector3[12];

        Waypointsdirection.Add(waypointsLeft.ToList());
        Waypointsdirection.Add(waypointsRight.ToList());

        direction = Waypointsdirection[Random.Range(0, Waypointsdirection.Count)];
        if (direction == Waypointsdirection[0])
        {
            for (int i = 0; i < direction.Count; i++)
            {
                LeftwayPoint1[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                LeftwayPoint2[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                LeftwayPoint3[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                LeftwayPoint4[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                LeftwayPoint5[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                LeftwayPoint6[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                LeftwayPoint7[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                LeftwayPoint8[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                LeftwayPoint9[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                LeftwayPoint10[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                LeftwayPoint11[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);

            }
        }
        else
        {
            for (int i = 0; i < direction.Count; i++)
            {
                RightwayPoint1[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                RightwayPoint2[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                RightwayPoint3[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                RightwayPoint4[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                RightwayPoint5[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                RightwayPoint6[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                RightwayPoint7[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                RightwayPoint8[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                RightwayPoint9[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                RightwayPoint10[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);
                RightwayPoint11[i] = FindLocation(direction[i].transform.position, mindistance, maxdistance);


            }

        }

        WayLeft.Add(LeftwayPoint1.ToList());
        WayLeft.Add(LeftwayPoint2.ToList());
        WayLeft.Add(LeftwayPoint3.ToList());
        WayLeft.Add(LeftwayPoint4.ToList());
        WayLeft.Add(LeftwayPoint5.ToList());
        WayLeft.Add(LeftwayPoint6.ToList());
        WayLeft.Add(LeftwayPoint7.ToList());
        WayLeft.Add(LeftwayPoint8.ToList());
        WayLeft.Add(LeftwayPoint9.ToList());
        WayLeft.Add(LeftwayPoint10.ToList());
        WayLeft.Add(LeftwayPoint11.ToList());

        WayRight.Add(RightwayPoint1.ToList());
        WayRight.Add(RightwayPoint2.ToList());
        WayRight.Add(RightwayPoint3.ToList());
        WayRight.Add(RightwayPoint4.ToList());
        WayRight.Add(RightwayPoint5.ToList());
        WayRight.Add(RightwayPoint6.ToList());
        WayRight.Add(RightwayPoint7.ToList());
        WayRight.Add(RightwayPoint8.ToList());
        WayRight.Add(RightwayPoint9.ToList());
        WayRight.Add(RightwayPoint10.ToList());
        WayRight.Add(RightwayPoint11.ToList());
        if (direction == Waypointsdirection[0])
        {
            selectpath = WayLeft[Random.Range(0, WayLeft.Count)];
        }
        else
        {
            selectpath = WayRight[Random.Range(0, WayRight.Count)];
        }


        targetPosition = selectpath[0];


    }

    private void Update()
    {

        if (lastposition==true)
        {
            return;
        }
        if (gameManager.gameStatus!=GameStatus.start)
        {
            return;
        }
        Move();
    }

    private void Move()
    {

        transform.position = Vector3.MoveTowards(transform.position, targetPosition , moveSpeed * Time.deltaTime);
        if (transform.position.z == selectpath[waypointIndex].z)
        {

            if (waypointIndex == selectpath.Count-1)
                lastposition = true;
            else
            {
                waypointIndex++;
                targetPosition = selectpath[waypointIndex];
            }

        }

    }
    public static Vector3 FindLocation(Vector3 from, float mindistance, float maxdistance)
    {

        float x = 0f;
        float z = 0f;
        float y = from.y;

        Vector3 newposition = new Vector3(x, y, z);

            x = UnityEngine.Random.Range(mindistance, maxdistance);
           
            z = UnityEngine.Random.Range(mindistance, maxdistance);
           
            newposition.x = ( (from.x + x));
            newposition.z = ( (from.z + z));
        //Debug.Log(newposition);
        return newposition;
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag =="Ocean")
        {

            gameObject.GetComponent<ObstacleCharacter>().enabled = false;
            gameObject.SetActive(false);
            gameObject.GetComponent<ObstacleCharacter>().enabled = true;
            gameObject.transform.position = new Vector3(0, 16, -29);
            gameObject.SetActive(true);

            targetPosition = selectpath[0];
            waypointIndex = 0;
        }
        if (collision.gameObject.tag == "Horizantal1")
        {
            rb.AddForce(Vector3.back * thrust);
        }
        if (collision.gameObject.tag == "Horizantal2")
        {
            rb.AddForce(Vector3.back * thrust);
        }
        if (collision.gameObject.tag == "Horizantal3")
        {
            rb.AddForce(Vector3.back * thrust);
        }
        if (collision.gameObject.tag == "Horizantal4")
        {
            rb.AddForce(Vector3.back * thrust);
        }
        if (collision.gameObject.tag == "Rotater")
        {
            rb.AddForce(Vector3.left * thrust);
            rb.AddForce(Vector3.back * thrust / 2);
        }
        if (collision.gameObject.tag == "MoveStick1")
        {
            rb.AddForce(Vector3.left * thrust/4);
            rb.AddForce(Vector3.back * thrust / 2);
        }
        if (collision.gameObject.tag == "MoveStick2")
        {
            rb.AddForce(Vector3.right * thrust / 4);
            rb.AddForce(Vector3.back * thrust / 2);
        }
    }
}

