﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HalfDonutScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Moveright();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Moveright()
    {
        if (gameObject.transform.tag == "HalfDonut2")
        {
            gameObject.transform.DOMove(new Vector3(-8.1f, 20.1f, 204.1f), 2).OnComplete(Moveleft2);
        }
        if (gameObject.transform.tag == "HalfDonut1")
        {
            gameObject.transform.DOMove(new Vector3(8.4f, 20.1f, 198), 2).OnComplete(Moveleft1);
        }
        
    }
    public void Moveleft1()
    {
        gameObject.transform.DOMove(new Vector3(12.1f, 20.1f, 198), 3).OnComplete(Moveright);
    }
    public void Moveleft2()
    {
        gameObject.transform.DOMove(new Vector3(-12.2f, 20.1f, 204.1f), 3).OnComplete(Moveright);
    }
}
