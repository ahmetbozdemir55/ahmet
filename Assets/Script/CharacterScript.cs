﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CharacterScript : MonoBehaviour
{
    public UIManager uIManager;
    //public string name { get; set; }
    //public int activeWaypointIndex { get; set; }
    //public float distanceToWaypoint { get; set; }

    //public override string ToString()
    //{
    //    return activeWaypointIndex + "__" + name;
    //} 
    public Rigidbody rb;
    public GameObject plane;
    public Camera camera;
    public GameObject OpponentCharacter;
    public GameObject FinishPanel;
    public GameObject Scractch;
    public float thrust ;
    public GameObject MaterialPlane;
    public GameObject FillPlane;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //gameObject.transform.Rotate(new Vector3(0, 0, 0));
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "ColiderFinish")
        {

            camera.transform.SetParent(null);
            //MaterialPlane.gameObject.SetActive (true);
            //FillPlane.gameObject.SetActive(true);
            camera.transform.DOMove(new Vector3(0.1f, 24.09f, 232.85f), 2f);
            camera.transform.DORotate(new Vector3(-2.07f, 0, 0), 2f);
            OpponentCharacter.gameObject.SetActive(false);
            Invoke("Delay", 1.4f);
            Scractch.GetComponent<Scractch>().enabled = true;
            //FinishPanel.GetComponent<FinishPanelScript>().enabled = true;
            gameObject.GetComponent<InputManager>().enabled = false;

            //plane.gameObject.GetComponent<PaintTable>().enabled = true;
        }
        if (collision.gameObject.tag == "Ocean")
        {
            gameObject.GetComponent<CharacterScript>().enabled = false;
            gameObject.SetActive(false);
            gameObject.GetComponent<CharacterScript>().enabled = true;
            gameObject.transform.position = new Vector3(0, 16, -29);
            gameObject.SetActive(true);
        }
        if (collision.gameObject.tag == "Horizantal1")
        {
            rb.AddForce(Vector3.back*thrust);
        }
        if (collision.gameObject.tag == "Horizantal2")
        {
            rb.AddForce(Vector3.back * thrust);
        }
        if (collision.gameObject.tag == "Horizantal3")
        {
            rb.AddForce(Vector3.back * thrust);
        }
        if (collision.gameObject.tag == "Horizantal4")
        {
            rb.AddForce(Vector3.back * thrust);
        }
        if (collision.gameObject.tag == "Rotater")
        {
            rb.AddForce(Vector3.left * thrust);
            rb.AddForce(Vector3.back * thrust/2);
        }
        if (collision.gameObject.tag == "MoveStick1")
        {
            rb.AddForce(Vector3.left * thrust / 4);
            rb.AddForce(Vector3.back * thrust / 2);
        }
        if (collision.gameObject.tag == "MoveStick2")
        {
            rb.AddForce(Vector3.right * thrust / 4);
            rb.AddForce(Vector3.back * thrust / 2);
        }
    }
    public void Delay()
    {
        uIManager.GameFinish();
    }
}
