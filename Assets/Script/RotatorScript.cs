﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorScript : MonoBehaviour
{
    public int rotatingRotation;
    // Start is called before the first frame update
    void Start()
    {
        rotatingRotation = Random.RandomRange(1, 4);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, rotatingRotation, 0));
    }
}
