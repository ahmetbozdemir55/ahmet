﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingPlatformScript : MonoBehaviour
{
    public int rotatingPlatformMovez;
    [SerializeField] private float rotSpeed;
    [SerializeField] private float rotSpeed1;
    [SerializeField] private float rotSpeed2;
    private Rigidbody rb;
    public List<float> rotatingfloat;
    public int i;

    // Start is called before the first frame update
    void Start()
    {
        rotatingfloat = new List<float>();
        rotSpeed = Random.RandomRange(-15, -10);
        rotSpeed1 = Random.RandomRange(15, 20);
        rotSpeed2 = Random.RandomRange(-15, -25);
        i = Random.RandomRange(0, 3);
        rotatingfloat.Add(rotSpeed);
        rotatingfloat.Add(rotSpeed1);
        rotatingfloat.Add(rotSpeed2);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //transform.Rotate(new Vector3(0, 0, rotatingPlatformMovez));
        //transform.Rotate(0, 0, Time.deltaTime * rotSpeed, Space.Self);
        //gameObject.GetComponent<Rigidbody>().rotation = Quaternion.Euler(0, 0, Time.deltaTime * rotSpeed*100);
        Quaternion deltaRotation = Quaternion.Euler(new Vector3(0, 0, rotatingfloat[i]) * Time.fixedDeltaTime);
        gameObject.GetComponent<Rigidbody>().MoveRotation(gameObject.GetComponent<Rigidbody>().rotation * deltaRotation);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            rb = collision.gameObject.GetComponent<Rigidbody>();
            //InvokeRepeating(nameof(LeftAddForce), 0, 0.01f);
            collision.gameObject.transform.SetParent(this.transform);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //CancelInvoke(nameof(LeftAddForce));
            collision.gameObject.transform.SetParent(null);
        }
    }
    private void LeftAddForce()
    {
        rb.AddForce(Vector3.left * 1.5f, ForceMode.Acceleration);
    }
}
//[SerializeField] private float rotSpeed;
//private Rigidbody rb;
//void Update()
//{
//    transform.Rotate(0, 0, Time.deltaTime * rotSpeed, Space.Self);
//}

//private void OnCollisionEnter(Collision collision)
//{
//    if (collision.gameObject.CompareTag("Player"))
//    {
//        rb = collision.gameObject.GetComponent<Rigidbody>();
//        InvokeRepeating(nameof(LeftAddForce), 0, 0.01f);
//    }
//}

//private void OnCollisionExit(Collision collision)
//{
//    if (collision.gameObject.CompareTag("Player"))
//    {
//        CancelInvoke(nameof(LeftAddForce));
//    }
//}

//private void LeftAddForce()
//{
//    rb.AddForce(Vector3.left * 100, ForceMode.Acceleration);
//}