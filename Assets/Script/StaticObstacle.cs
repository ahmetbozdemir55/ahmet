﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticObstacle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "ObstacleCharacter")
        {
            collision.gameObject.SetActive(false);
            collision.gameObject.transform.position = new Vector3(0, 16, -29);
            collision.gameObject.GetComponent<ObstacleCharacter>().enabled = false;
            collision.gameObject.SetActive(true);
            collision.gameObject.GetComponent<ObstacleCharacter>().enabled = true;

            
        }
    }

}
