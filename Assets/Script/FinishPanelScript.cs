﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
public class FinishPanelScript : MonoBehaviour
{
    public UIManager uIManager;
    public FinishPanelScript finishPanelScript;

    [SerializeField] private TextMeshProUGUI BrushCalculate;

    [Header("UI References")]
    [SerializeField] private Image Fill_Image;
    [SerializeField] private Image Bird;


    public float newCalculate;
    public float fillCaulcate;
    public float RectTransformCalculate;
    public GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        fillCaulcate = 0.2f;
        RectTransformCalculate = 208.3f;
    }

    // Update is called once per frame
    void Update()
    {
        newCalculate = fillCaulcate;

        float newProgressValue = Mathf.InverseLerp(0, 1, newCalculate);
        UbdateFillmage(newProgressValue);
        UbdateRectTransform(RectTransformCalculate);



        //Debug.Log(fillCaulcate);
        //Debug.Log(newObstacleObje);
        //Debug.Log(newProgressValue);


        BrushCalculate.text = "% " + (fillCaulcate * 100);
        if (fillCaulcate * 100 > 100)
        {
            BrushCalculate.text = "GREAT!!";
            uIManager.GameFinish(); 
            finishPanelScript.gameObject.GetComponent<FinishPanelScript>().enabled = false;
        }

        Invoke("Delay", 1.3f);
    }
    private void UbdateRectTransform(float value)
    {
        Bird.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, value, 0);
    }
    private void UbdateFillmage(float value)
    {
        Fill_Image.fillAmount = value;
        
    }
    public void Delay()
    {
        if (fillCaulcate>0)
        {
            fillCaulcate = fillCaulcate - 0.015f;
            RectTransformCalculate = RectTransformCalculate + 11.4f;
        }
    }
    public void FillButton()
    {
        fillCaulcate = fillCaulcate + 0.2f;
        RectTransformCalculate = RectTransformCalculate - 152;
    }
}
