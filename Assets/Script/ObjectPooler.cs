﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler instance;
    public List<GameObject> BrushPool;
    int BurshInstate = 2500;
    public GameObject brushcollider;

    public void Awake()
    {
        instance = this;

        BrushPool = new List<GameObject>();
        for (int i = 0; i < BurshInstate; i++)
        {
            GameObject newBurshpool = Instantiate(brushcollider);
            newBurshpool.gameObject.SetActive(false);
            BrushPool.Add(newBurshpool);
        }
    }
    public GameObject GetObject()
    {

        for (int i = 0; i < BrushPool.Count; i++)
        {
            if (!BrushPool[i].activeInHierarchy)
            {

                return BrushPool[i];
            }
        }
        GameObject instance = Instantiate(brushcollider);
        instance.gameObject.SetActive(false);
        BrushPool.Add(instance);
        return instance;
    }
}