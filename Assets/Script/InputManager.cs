﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public GameManager gameManager;
    public GameStatus gameStatus;
    public UIManager uIManager;

    [Header("Movement")]
    [SerializeField] private float speed;
    [SerializeField] private float forwardSpeed;


    private float mZCoord;
    private Vector3 mOffset;
    private bool isStart;

    private void Start()
    {
        isStart = false;

    }

    void FixedUpdate()
    {
        if (!isStart)
            return;

        transform.Translate(Vector3.forward * forwardSpeed * Time.deltaTime); // daime öne gitmesini sağlıyor


        if (Input.GetMouseButtonDown(0))
        {

            mZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;

            mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
        }

        if (Input.GetMouseButton(0))
        {

            Vector3 andPos = new Vector3((GetMouseAsWorldPoint().x + mOffset.x), transform.position.y, transform.position.z);

            transform.position = Vector3.Lerp(transform.position, andPos, speed * Time.deltaTime);

            transform.position = new Vector3(Mathf.Clamp(transform.position.x, -33.8f, 33.8f), transform.position.y, transform.position.z); // platformun sağ ve sol sınırını belirle 

        }



    }

    private Vector3 GetMouseAsWorldPoint()
    {

        Vector3 mousePoint = Input.mousePosition;


        mousePoint.z = mZCoord;

        // Convert it to world points
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    public void OnClickStart()
    {

        isStart = true;
        // istersen aniamsyonu başlatırsın 
        uIManager.GameStart();

    }

}